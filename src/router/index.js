import {
    createRouter,
    createWebHashHistory
} from 'vue-router'


const routes = [{
        path: '/',
        name: 'Home',
        component: () =>
            import ('../views/Home.vue')
    },
    {
        path: '/about',
        name: 'About',
        component: () =>
            import ('../views/About.vue')
    },
    {
        path: "/classify",
        name: "classify",
        component: () =>
            import ("../views/classifys.vue")
    },
    {
        path: "/detail/:id",
        name: "detail",
        component: () =>
            import ("../components/classify/Detailclassify.vue")

    },
    {
        path: "/send",
        component: () =>
            import ("../components/my/Send.vue")

    },
    {
        path: "/user",
        component: () =>
            import ("../components/homeuser/Homeuser.vue")

    },
    {
        path: "/search",
        component: () =>
            import ("../components/classify/Search.vue")
    },
    {
        path: "/find",
        name: "find",
        component: () =>
            import ("../views/find/Community.vue"),
        children: [{
                path: "/shopstreet",
                component: () =>
                    import ("../components/find/ShopStreet.vue"),


            },
            {
                path: "/community",
                component: () =>
                    import ("../components/find/Community.vue")
            },
            {
                path: "/vido",
                component: () =>
                    import ("../components/find/vido.vue")
            }

        ]

    },
    {
        path: "/vidoDetail/:id",
        name: "vido",
        component: () =>
            import ("../components/find/Inside/Vido.vue")
    },
    {
        path: "/comminside/:id",
        name: "comminside",
        component: () =>
            import ("../components/find/Inside/Community.vue")
    },

    {
        path: "/shopping",
        name: "shopping",
        component: () =>
            import ("../views/shopping/Cat.vue"),
    },
    {
        path: "/shopInCat",
        name: "shopInCat",
        component: () =>
            import ("../views/shopping/shopInCat.vue"),
    },

    {
        //进店
        path: "/storedetails",
        component: () =>
            import ("../components/find/Storedetails.vue")
    },
    {
        path: "/open",
        name: "open",
        component: () =>
            import ("../components/home/newcommponents/list.vue"),
    },
]
const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router