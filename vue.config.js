module.exports = {
    devServer: {
      proxy: {
        '/api': {
          target: 'https://x.dscmall.cn',
          ws: true,
          changeOrigin: true
        },
      }
    }
  }