import axios from "../../common/axiossecond.js"
const shopstreet = {
    state() {
        return {
            setShopstreet: [],
            setEnterstore: [],
            setPupop: [],
        }
    },
    mutations: {
        setShopstreet(state, { data }) {
            state.setShopstreet = data
        },
        setEnterstore(state, { data }) {
            state.setEnterstore = data
        },
        setPupop(state, { data }) {
            state.setPupop = data
        },

    },
    actions: {
        setShopstreetr({ commit }, state) {
            return new Promise((r, j) => {
                axios.post("/api/shop/catlist", {

                }).then(({ data: { data, status } }) => {
                    if (status == "success") {
                        // console.log(data);
                        r(data)
                        commit({
                            type: "setShopstreet",
                            data: data
                        })
                    } else {
                        alert(msg)
                    }
                })

            })
        },
        setEnterstorer({ commit }, state) {
            return new Promise((r, j) => {
                axios.post("/api/shop/catshoplist", {

                }).then(({ data: { data, status } }) => {
                    if (status == "success") {
                        console.log(data);
                        r(data)
                        commit({
                            type: "setEnterstore",
                            data: data
                        })
                    } else {
                        alert(msg)
                    }
                })
            })
        },
        setPupopr({ commit }, state) {
            return new Promise((r, j) => {
                axios.post("/api/visual/product", {

                }).then(({ data: { data, status } }) => {
                    if (status == "success") {
                        console.log(data);
                        r(data)
                        commit({
                            type: "setPupop",
                            data: data
                        })
                    } else {
                        alert(msg)
                    }
                })

            })
        },
    }
}


export default shopstreet