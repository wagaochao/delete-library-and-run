import { createStore } from 'vuex'

import shopModule from "./shopCat/shopCat.js"
import homeModule from "./home/homeModule.js"
import shopInModule from "./shopCat/shopIn.js"
import community from "./find/community.js"
import vidolist from "./find/vido.js"
import goods from "../store/classify/classify.js"
import shopstreet from "../store/find/ShopStreet.js"

export default createStore({
    state: {
        
    },
    mutations: {},
    actions: {},
    modules: {
        shopping: shopModule,
        find: community,
        classify: goods,
        vido: vidolist,
        shopIn: shopInModule,
        home: homeModule,
        shop: shopstreet,
    }
})