import axios from "../../common/axiossecond.js"


const Community = {
    state() {
        return {
            
        }
    },
    mutations: {
        
    },
    actions: {
        getFindCommlist({},{size,page}) {
            return new Promise((res,rej)=>{
                axios.get("/api/discover/find_list", {
                    params: {
                        size,
                        page
                    }
                }).then(({
                    data: {
                        data,
                        status
                    }
                }) => {
                    if (status == "success") {
                        res(data)
                    } else {
                        rej(status)
                    }
                })
            })
        },

        getCommDetail({},{dis_id}){
            return new Promise((res,rej)=>{
                axios.get("/api/discover/find_detail", {
                    params: {
                        dis_id
                    }
                }).then(({
                    data: {
                        data,
                        status
                    }
                }) => {
                    if (status == "success") {
                        res(data)
                    } else {
                        rej(status)
                    }
                })
            })
        },

        getReply({},{dis_id,page,size}){
            return new Promise((res,rej)=>{
                axios.get("/api/discover/find_reply_comment", {
                    params: {
                        dis_id,
                        page,
                        size
                    }
                }).then(({
                    data: {
                        data,
                        status
                    }
                }) => {
                    if (status == "success") {
                        res(data)
                    } else {
                        rej(status)
                    }
                })
            })
        },
        gethomegoods({},{page,size,type1}){
            return new Promise((res,rej)=>{
                axios.get("/api/goods/type_list", {
                    params: {
                        type1,
                        page,
                        size
                    }
                }).then(({
                    data: {
                        data,
                        status
                    }
                }) => {
                    if (status == "success") {
                        res(data)
                    } else {
                        rej(status)
                    }
                })
            })
        }
    }
}
export default Community