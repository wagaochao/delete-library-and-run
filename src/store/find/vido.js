import axios from "axios"
const video = {
    state() {
        return {

        }
    },
    mutations: {

    },
    actions: {
        getvideo({}, {
            size,
            page
        }) {
            return new Promise((res)=>{
                axios.post("/api/goods/goodsvideo", {
                        size,
                        page
                }).then(({
                    data: {
                        data,
                        status
                    }
                }) => {
                    if (status == "success") {
                        res(data)
                    }
                })
            })
        }
    }
}

export default video