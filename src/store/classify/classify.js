// import { createStore } from "vuex"
import axios from "../../common/axiossecond.js"
const goods = ({
    state() {
        return {
            classifygoods: [],
            classifygoodstwo: [],
            classifygoodsthree: [],
            classifygoodsfour: [],
            classifygoodsfive: [],
            classifygoodssix: [],
            classifygoodsseven: [],

        }

    },
    mutations: {
        setgoods(state, { data }) {
            state.classifygoods = data
        },
        setgoodstwo(state, { data }) {
            state.classifygoodstwo = data

        },
        setgoodsthree(state, { data }) {
            state.classifygoodsthree = data

        },
        setgoodsfour(state, { data }) {
            state.classifygoodsfour = data

        },
        setgoodsfive(state, { data }) {
            state.classifygoodsfive = data

        },
        setgoodssix(state, { data }) {
            state.classifygoodssix = data

        },
        setgoodsseven(state, { data }) {
            state.classifygoodsseven = data

        }

    },
    actions: {
        // 第一个页面接口
        getgoods({ commit }) {
            axios.get("/api/catalog/list/858", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoods",
                        data
                    })
                }
            })
        },
        // 第二个页面接口
        getgoodsTwo({ commit }) {
            axios.get("/api/catalog/list/6", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoodstwo",
                        data
                    })
                }
            })
        },
        // 第三个页面接口
        getgoodsThree({ commit }) {
            axios.get("/api/catalog/list/8", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoodsthree",
                        data
                    })
                }
            })
        },
        // 第四个页面接口
        getgoodsFour({ commit }) {
            axios.get("/api/catalog/list/3", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoodsfour",
                        data
                    })
                }
            })
        },
        // 第五个页面接口
        getgoodsFive({ commit }) {
            axios.get("/api/catalog/list/4", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoodsfive",
                        data
                    })
                }
            })
        },

        // 第六个页面接口
        getgoodsSix({ commit }) {
            axios.get("/api/catalog/list/5", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoodssix",
                        data
                    })
                }
            })
        },
        // 第七个页面接口
        getgoodsSeven({ commit }) {
            axios.get("/api/catalog/list/860", {

            }).then(({ data: { data, status } }) => {
                // console.log(data);
                if (status == "success") {
                    commit({
                        type: "setgoodsseven",
                        data
                    })
                }
            })
        },

        // 细节跳转页面接口
        detailclassify({}, { cat_id, keywords, sort, min, max }) {
            // console.log(sort);

            return new Promise((resolve, reject) => {
                axios.post("/api/catalog/goodslist", { cat_id, keywords, sort, min, max }).then(({ data: { data, status } }) => {
                    // console.log(data);
                    if (status == "success") {
                        resolve(data)
                    } else {
                        reject(msg)
                    }
                })
            })
        },

        // 厨房电器接口
        userelectrical({}, { cat_id }) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/visual/visual_second_category?cat_id=${cat_id}`).then(({ data: { data, status, msg } }) => {
                    console.log(data);
                    if (status == "success") {
                        resolve(data)
                    } else {
                        reject(msg)
                    }

                })
            })
        }


    }
})
export default goods