import axios from "../../common/axiossecond.js"
const shopInModule={
    state(){
        return{
            ShopInList:[],
            ShopIn:[],
            cat:[]           
        }
    },
    mutations:{
        setShopInList(state,{data}){
            state.ShopInList=data
        },
        setShopIn(state,{data}){
            state.ShopIn=data
        },
        setCatData(state,{data}){
            state.cat=data
        },
    },
    actions:{
        getCatData({commit},data){
            commit({
                type:"setCatData",
                data
            })
        },
       
        getInList({commit},{goods_id}){
            axios.post("/api/goods/show",{
                goods_id,
                warehouse_id: 0,
                area_id: 0,
                is_delete: 0,
                is_on_sale: 1,
            }).then(({data:{data,status}})=>{
                if(status=="success"){
                    commit({
                        type:"setShopInList",
                        data
                    })
                }else{
                    alert(msg)
                }
            })
        },
        getCity({commit}){
            return new Promise((reslove,reject)=>{
                axios.get("/api/misc/region?region=310101&level=4",{
                 
                }).then(({data})=>{
                    reslove(data)
                })
            })
             
        },
        getAdd({commit},{goods_id}){
            console.log(goods_id);
            return new Promise((reslove,reject)=>{
                axios.post("/api/cart/add",{
                    goods_id: goods_id,
                    num: 1
                }).then(({data})=>{
                    // console.log(data);
                    reslove(data)
                })
            })           
        }, 
        getRecommend({commit}){
            return new Promise((reslove,reject)=>{
                axios.post("/api/catalog/goodslist",{
                    page: 1,
                    size: 30
                }).then(({data:{data}})=>{
                    console.log(data);
                    reslove(data)
                })
            })            
        },
        getGoods({commit},{goods_id}){
            return new Promise((reslove,reject)=>{
                axios.post(`/api/comment/goods?goods_id=${goods_id}&rank=all&page=1&size=2`,{
                }).then(({data:{data}})=>{
                    reslove(data)
                })
            })            
        },
        getNum({commit},{goods_id}){
            return new Promise((reslove,reject)=>{
                axios.post(`/api/comment/title?goods_id=${goods_id}`,{
                }).then(({data:{data}})=>{
                    reslove(data)
                })
            })            
        }
        // getIn({commit},{goods_id,ru_id}){
        //     axios.get(`/coupon/goods?goods_id=${goods_id}&ru_id=${ru_id}`,{
        //     }).then(({data:{data,status}})=>{
        //         console.log(data);
        //         if(status=="success"){
        //             commit({
        //                 type:"setShopInList",
        //                 data
        //             })
        //         }else{
        //             alert(msg)
        //         }
        //     })
        // }
    }
  }
  export default shopInModule