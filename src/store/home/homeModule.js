import axiossecond from '../../common/axiossecond'
export default {
    state() {
        return {
            viewData: null,
            articleData: null,
            seckillList: null
        }
    },
    getters: {
        haveViewData(state) {
            if (state.viewData === null) {
                return false
            }
            return true
        },
        findViewData: (state) => (param) => {
            return state.viewData.filter((item) => {
                return item.module === param
            })
        },
        haveArticleData(state) {
            if (state.articleData === null) {
                return false
            }
            return true
        },
        haveSeckillList(state) {
            if (state.seckillList === null) {
                return false
            }
            return true
        },
    },
    mutations: {
        setViewData(state, list) {
            state.viewData = list
        },
        setArticleData(state, list) {
            state.articleData = list
        },
        setSeckillList(state, list) {
            state.seckillList = list
        }
    },
    actions: {
        getView({ commit }) {
            axiossecond.post("/api/visual/view", {
                id: 3,
                type: "index",
                device: "h5",
            })
                .then((res) => {
                    const list = JSON.parse(res.data.data.data)
                    commit('setViewData', list)
                })
                .catch((err) => {
                    console.log("view获取出错");
                });
        },
        getArticle({ commit }) {
            axiossecond.post('/api/visual/article', {
                cat_id: 20,
                num: 10
            })
                .then((res) => {
                    commit('setArticleData', res.data.data)
                })
                .catch((err) => {
                    console.log('article获取出错');
                })
        },
        getSeckillList({ commit }, { id, tomorrow }) {
            axiossecond.post('/api/visual/visual_seckill', { id, tomorrow })
                .then((res) => {
                    commit('setSeckillList', res.data.seckill_list)
                })
                .catch((err) => {
                    console.log('seckillList获取出错');
                })
        }


    }
}