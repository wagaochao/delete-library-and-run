import axios from "../../common/axios.js"
const shopModule={
    state(){
        return{
            ShopCatList:[],
        }
    },
    mutations:{
        setShopCatList(state,{data}){
            state.ShopCatList=data
        },
        setShopInList(state,{data}){
            state.ShopInList=data
        }
    },
    actions:{
        getShopCatList({commit},state){
            axios.post("/api/goods/goodsguess",{
              page: 1,
              size: 10
            }).then(({data:{data,status}})=>{
                if(status=="success"){
                    // console.log(data);
                    commit({
                        type:"setShopCatList",
                        data:data
                    })
                }else{
                    alert(msg)
                }
            })
        },
    }
  }
  export default shopModule