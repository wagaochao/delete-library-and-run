import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
const app = createApp(App)


import 'vant/lib/index.css';
// Vue.use(Vant);
app.use(store).use(router).use(Vant).mount('#app')